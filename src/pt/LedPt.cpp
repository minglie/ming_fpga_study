#include <Arduino.h>
#include "AtShell.h"
#include "Protothread.h"
#include "main.h"
#include "Main_constant.h"

class LedPt : public Protothread {
    void Init() override {
        g_pt_Handel.led= Protothread::M_npt;
        AT_println("LedPt init");
    }

    bool Run() override {
        WHILE(1) {
            PT_WAIT_UNTIL(PopInData());

            Notify(this, ProtothreadNotifyEvent{55,3,0});

            digitalWrite(LED2, !digitalRead(LED2));
        }
        PT_END();
    }

    void OnRecvNotify(Protothread *source, ProtothreadNotifyEvent evt) override{
        AT_aprintln("LedPt OnRecvNotify %d", evt.code);
    }
};


static LedPt s_ledpt;