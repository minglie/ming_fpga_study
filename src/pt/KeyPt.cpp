#include <Arduino.h>
#include "AtShell.h"
#include "Protothread.h"
#include "ManKey.h"
#include "main.h"
#include "Main_constant.h"

static int keyPinRead(uint8_t id) {
    return !digitalRead(KEY0);
}

static void onKeyEvent(uint32_t ms, ManKeyEventCode manKeyEventCode) {
    switch (manKeyEventCode.one.evtCode) {
        case MAN_KEY_EVT_DOWN:
            Serial.println("down");
            Protothread::PushIndata(Protothread::M_pts[g_pt_Handel.led], 1);
            break;
        case MAN_KEY_EVT_UP    :
            Serial.println("up");
            break;
        case MAN_KEY_EVT_CLICK        : {
            Serial.println("click");
            break;
        }
        case MAN_KEY_EVT_DBL_CLICK:
            Serial.println("double click");
            break;
        case MAN_KEY_EVT_PRESSING:
            Serial.println("short down");
            break;
        case MAN_KEY_EVT_LONG_CLICK    :
            Serial.println("long click");
            break;
    }

}


class KeyPt : public Protothread {



    void Init() override {
        g_pt_Handel.key= Protothread::M_npt;
        AT_println("KeyPt init");
        ManKey::Create(1);
        ManKey::pinRead = keyPinRead;
        ManKey::onEvent = onKeyEvent;
    }

    bool Run() override {
        PtOsDelayMs(10);
        ManKey::OnTickAll(millis());
        return false;
    }
};


static KeyPt s_keyPt;