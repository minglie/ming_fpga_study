#include "AtShell.h"
#include <Arduino.h>
#include "Protothread.h"
#include "Main_constant.h"
#include "main.h"


class MslPt : public Protothread {
    void Init() override{
        g_pt_Handel.msl= Protothread::M_npt;
        AT_println("MslPt init");
    }

    bool Run() override{
        digitalWrite(LED4, !digitalRead(LED4));
        PtOsDelayMs(100);
        return false;
    }
};



static MslPt s_mslPt;