#include <Arduino.h>
#include "bsp_atshell.h"
#include "Protothread.h"
#include "bsp.h"

void HAL_TIM_PeriodElapsedCallback()
{
    Protothread::PollAndRun(millis());
}


void setup11() {
    BspInit();
    bsp_atshell_init();
    bsp_timer_set_callback(HAL_TIM_PeriodElapsedCallback);
    //启动PT协程
    Protothread::AllStart();
}


void loop11() {

    static uint32_t last_dis_ms=0;
    long ms= millis();
    bsp_atshell_run(ms);
    if(ms-last_dis_ms>500){
        last_dis_ms=ms;
    }
}