//
// Created by LangJie on 2024/10/20.
//

#include "bsp.h"
#include <Arduino.h>



void BspInit(void){

    bsp_timer_init();

    //init key
    pinMode(KEY0, INPUT_PULLUP);

    //init led
    pinMode(LED0, OUTPUT);
    pinMode(LED1, OUTPUT);
    pinMode(LED2, OUTPUT);
    pinMode(LED3, OUTPUT);
    pinMode(LED4, OUTPUT);


//    digitalWrite(LED0, 0);
//    digitalWrite(LED1, 0);
//    digitalWrite(LED2, 0);
//    digitalWrite(LED3, 0);
//    digitalWrite(LED4, 0);
//    digitalWrite(LED5, 0);
//    digitalWrite(LED6, 0);
//    digitalWrite(LED7, 0);


}