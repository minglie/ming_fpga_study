#include <Arduino.h>
#include "AtShell.h"
#include "bsp_atshell.h"
#include "bsp.h"
static int user_at_write(uint8_t* srcBuf, uint32_t toSendLen,uint32_t timeout) {
    return Serial.write(srcBuf, toSendLen);
}

//测试 函数
static int test_01(int argc, char** argv) {
    return 0;
}

void bsp_atshell_init(void){
    Serial.begin(115200);
    //初始化AtShell
    at_init(user_at_write);
    //命令导出,控制台输入test01 则执行 test_01函数
    AT_SHELL_EXPORT(test01, test888 ,test_01);
    at_show_version();
    AT_EXEC("help");
}

void bsp_atshell_run(uint32_t ms){
    int len=  Serial.available();
    if(len>0) {
        Serial.readBytes((uint8_t *)AT_m_buf, len);
        at_import((uint8_t *) AT_m_buf, len, ms);
    }
}