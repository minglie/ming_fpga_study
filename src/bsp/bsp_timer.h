#ifndef __bsp_timer_H
#define __bsp_timer_H


#include "stdint.h"

#ifdef __cplusplus
extern "C" {
#endif

void bsp_timer_init(void);
void bsp_timer_set_callback(void (*OnFun)(void));

#ifdef __cplusplus
}
#endif

#endif
