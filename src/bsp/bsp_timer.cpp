#include <Arduino.h>
#include <avr/sleep.h>
#include "bsp_timer.h"

typedef struct {
    void (*Fun)(void);
} PeriodElapsedCallback_TypeDef;


static PeriodElapsedCallback_TypeDef _onPeriodElapsedFun;

void bsp_timer_init(void) {
    TCCR1A = 0; // Normal port operation, OC1A/OC1B disconnected.
    TCCR1B = 0; // Normal port operation, OC1A/OC1B disconnected.
    TCNT1 = 0;  // set counter to 0.
    OCR1A = 15999;
    TCCR1B |= (1 << WGM12);  // use wave genreation
    TCCR1B |= (1 << CS10);   // use system clock
    TIMSK1 |= (1 << OCIE1A); // Timer/Counter 1, Output Compare A Match Interrupt Enable

}

void bsp_timer_set_callback( void (*OnFun)(void)){
    _onPeriodElapsedFun.Fun=OnFun;
}

ISR(TIMER1_COMPA_vect)
{
    if( _onPeriodElapsedFun.Fun != NULL){
        _onPeriodElapsedFun.Fun();
    }
}

