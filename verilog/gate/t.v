
module wokwi(
  input       X0,
  input       X1,
  output      Y0
);


  assign  Y0 = X0 & X1;

endmodule

