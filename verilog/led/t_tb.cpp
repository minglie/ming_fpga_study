#include <Arduino.h>

int ledPin = 12;  // LED connected to digital pin 13
int inPin =2;    // pushbutton connected to digital pin 7
int val = 0;


void setup() {
    Serial.begin(9600);
    pinMode(ledPin, OUTPUT);  // sets the digital pin 13 as output
    pinMode(inPin, INPUT);    // sets the digital pin 7 as input
    Serial.println("aa33a\n");
}

/**
   The main game loop
*/
void loop() {
    delay(100);
    val = digitalRead(inPin);   // read the input pin
    digitalWrite(ledPin, val);  // sets the LED to the button's value
    //Serial.println(val);
}
