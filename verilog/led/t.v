
module wokwi (
  input wire IN,
  output wire OUT
);

  // put your logic here, e.g.:
  assign OUT = ~IN;

endmodule